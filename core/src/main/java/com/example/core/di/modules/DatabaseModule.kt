package com.example.core.di.modules

import android.content.Context
import androidx.room.Room
import com.example.core.BuildConfig
import com.example.core.data.db.TestDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context) = Room.databaseBuilder(
        context,
        TestDataBase::class.java,
        BuildConfig.DATABASE_NAME
    ).build()


    @Provides
    @Singleton
    fun provideUserDao(appDatabase: TestDataBase) = appDatabase.getUserDao()

}
