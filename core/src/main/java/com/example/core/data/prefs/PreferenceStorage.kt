package com.example.core.data.prefs

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import androidx.annotation.WorkerThread
import androidx.core.content.edit
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.test.common.data_android.entity.UserEntity

import com.test.common.data_android.pref.PreferenceStorage
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


/**
 * [PreferenceStorage] impl backed by [android.content.SharedPreferences].
 */
@FlowPreview
@ExperimentalCoroutinesApi
@Singleton
class SharedPreferenceStorage @Inject constructor(
    @ApplicationContext context: Context
) : PreferenceStorage {


    private val prefs: Lazy<SharedPreferences> = lazy { // Lazy to prevent IO access to main thread.
        context.applicationContext.getSharedPreferences(
            PREFS_NAME, MODE_PRIVATE
        )
    }


    override var session by UserPreference(
        prefs, PREF_LOCATION,
        UserEntity()
    )

    companion object {
        const val PREFS_NAME = "test"
        const val PREF_LOCATION = "pref_user"
    }


}


class UserPreference(
    private val preferences: Lazy<SharedPreferences>,
    private val name: String,
    private val defaultValue: UserEntity
) : ReadWriteProperty<Any, UserEntity> {

    @WorkerThread
    override fun getValue(thisRef: Any, property: KProperty<*>): UserEntity {
        val data = preferences.value.getString(name, null)
        return data?.toUserEntity() ?: defaultValue
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: UserEntity) {
        preferences.value.edit { putString(name, value.toGson()) }
    }

    private fun String.toUserEntity(): UserEntity {
        return Gson().fromJson(this, UserEntity::class.java)
    }

    private fun UserEntity.toGson(): String {
        return Gson().toJson(this)
    }
}

