package com.example.core.data.prefs

import com.test.common.data_android.pref.PreferenceStorage
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
abstract class PreferenceStorageModule {

    @ExperimentalCoroutinesApi
    @FlowPreview
    @Binds
    @Singleton
    abstract fun providePreferenceStorage(sharedPreferenceStorage: SharedPreferenceStorage): PreferenceStorage

}