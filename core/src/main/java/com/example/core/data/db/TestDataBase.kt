package com.example.core.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.test.common.data_android.dao.UserDao
import com.test.common.data_android.entity.UserEntity

@Database(
    entities = [
        UserEntity::class
    ],
    version =1,
    exportSchema = false
)
@TypeConverters(LocationTypeConverters::class)
abstract class TestDataBase : RoomDatabase() {
    abstract fun getUserDao(): UserDao
}