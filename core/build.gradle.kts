import dependencies.Libs
import dependencies.Test
import dependencies.Tools


plugins {
    GradlePluginId.apply {
        id(ANDROID_LIBRARY)
        id(KOTLIN_ANDROID)
        id(PARCELIZE)
        id(KOTLIN_KAPT)
        id(KOTLIN_Hilt)
    }
}

android {
    GradleVersionConfig.apply {
        compileSdkVersion(COMPILE_SDK_VERSION)
        buildToolsVersion = BUILD_TOOLS_VERSION

        defaultConfig {
            minSdkVersion(MIN_SDK_VERSION)
            targetSdkVersion(TARGET_SDK_VERSION)
            versionCode = VERSION_CODE
            versionName = VERSION_NAME
            testInstrumentationRunner = TEST_INSTRUMENTATION_RUNNER


            buildConfigField("String", "DATABASE_NAME", "\"test\"")
        }
        android.buildFeatures.dataBinding = true
        compileOptions.sourceCompatibility = JavaVersion.VERSION_1_8
        compileOptions.targetCompatibility = JavaVersion.VERSION_1_8
        kotlinOptions {
            jvmTarget = JavaVersion.VERSION_1_8.toString()
        }
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))


    implementation(project(Modules.Common.dataAndroid))
    implementation(project(Modules.Common.thread))

    implementation(Libs.AndroidX.Room.core)
    implementation(Libs.AndroidX.paging)
    implementation(Libs.Network.OkHttp.core)
    implementation(Libs.Network.OkHttp.logger)
    implementation(Libs.Network.Retrofit.core)
    implementation(Libs.Network.Retrofit.gsonConverter)
    implementation(Libs.AndroidX.LifeCycle.commonJava8)
    implementation(Libs.AndroidX.LifeCycle.liveData)
    implementation(Libs.AndroidX.LifeCycle.runtime)
    implementation(Libs.AndroidX.LifeCycle.viewModel)
    implementation(Libs.AndroidX.extensionsCore)
    implementation(Libs.Kotlin.kotlin_stdlib)
    implementation(Libs.AndroidX.LifeCycle.lifecycle_extensions)
    implementation(Libs.Kotlin.Coroutine.core)
    implementation(Libs.Kotlin.Coroutine.android)
    implementation(Libs.Stetho.okHttp)
    implementation(Libs.Stetho.core)
    implementation (Libs.DependencyInjection.Hilt.hilt)
    kapt (Libs.DependencyInjection.Hilt.compiler)
    kapt(Libs.AndroidX.Room.compiler)


}