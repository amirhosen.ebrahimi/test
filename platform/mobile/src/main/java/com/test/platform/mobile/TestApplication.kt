package com.test.platform.mobile


import android.app.Application
import com.example.core.BuildConfig
import com.facebook.stetho.Stetho
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber
// todo fix scope or coustom component hilt ?!
// todo hilt in modular
@HiltAndroidApp
class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
        }
    }

}