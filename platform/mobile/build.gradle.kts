import dependencies.Libs
import dependencies.Tools

// todo fix gradle
plugins {
    GradlePluginId.apply {
        id(ANDROID_APPLICATION)
        id(KOTLIN_ANDROID)
//        id(PARCELIZE) todo
        id(ANDROID_EXTENSIONS)
        id(KOTLIN_KAPT)
        id(KOTLIN_Hilt)
    }
}
kapt {
    correctErrorTypes = true
    useBuildCache = true

    javacOptions {
        option("-Xmaxerrs", 2000)
    }
}

android {
    GradleVersionConfig.apply {
        compileSdkVersion(COMPILE_SDK_VERSION)
        buildToolsVersion(BUILD_TOOLS_VERSION)

        defaultConfig {
//            applicationId = "com.test.mobile"
            minSdkVersion(MIN_SDK_VERSION)
            targetSdkVersion(TARGET_SDK_VERSION)
            versionCode = VERSION_CODE
            versionName = VERSION_NAME
            testInstrumentationRunner = TEST_INSTRUMENTATION_RUNNER
        }
        compileOptions.sourceCompatibility = JavaVersion.VERSION_1_8
        compileOptions.targetCompatibility = JavaVersion.VERSION_1_8
        kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
    kotlinOptions {
        jvmTarget = "1.8"
        freeCompilerArgs =
            freeCompilerArgs.toMutableList().apply { add("-Xopt-in=kotlin.RequiresOptIn") }
    }
    android.buildFeatures.dataBinding = true
    buildTypes {
        getByName("release") {
            isMinifyEnabled = BuildTypeDebug.isMinifyEnabled
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    dependencies {
        implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    }
}
dependencies {
    implementation(project(Modules.CORE))
    implementation(project(Modules.Feature.profiling))
    implementation(project(Modules.Common.mapper))
    implementation(project(Modules.Common.data))
    implementation(project(Modules.Common.dataAndroid))
    implementation(project(Modules.Common.dataSource))
    implementation(project(Modules.Common.useCase))
    implementation(project(Modules.Common.sdkExtensions))
    implementation(project(Modules.Common.thread))



    implementation(Libs.timber)
    implementation(Libs.AndroidX.fragmentKtx)
    implementation(Libs.AndroidX.fragment)
    implementation(Tools.leakCanary)
    implementation(Libs.AndroidX.LifeCycle.commonJava8)
    implementation(Libs.AndroidX.LifeCycle.liveData)
    implementation(Libs.AndroidX.LifeCycle.runtime)
    implementation(Libs.AndroidX.LifeCycle.viewModel)
    implementation(Libs.material)
    implementation(Libs.AndroidX.extensionsCore)
    implementation(Libs.Kotlin.kotlin_stdlib)
    implementation(Libs.AndroidX.LifeCycle.lifecycle_extensions)
    implementation(Libs.Scale.sdp)
    implementation(Libs.Scale.ssp)
    implementation(Libs.AndroidX.constraintLayout)
    implementation(Libs.AndroidX.Navigation.core)
    implementation(Libs.AndroidX.Navigation.ui)
    implementation(Libs.Stetho.okHttp)
    implementation(Libs.Stetho.core)
    implementation(Libs.AndroidX.paging)
    implementation(Libs.Network.OkHttp.core)
    implementation(Libs.Network.OkHttp.logger)
    implementation(Libs.Network.Retrofit.core)
    implementation(Libs.Network.Retrofit.gsonConverter)
    implementation(Libs.AndroidX.Room.core)
    implementation(Libs.DependencyInjection.Hilt.hilt)
    implementation(Libs.DependencyInjection.Hilt.ViewModel.hilt)

    kapt(Libs.DependencyInjection.Hilt.compiler)
    kapt(Libs.DependencyInjection.Hilt.ViewModel.compiler)
    kapt(Libs.AndroidX.Room.compiler)

}