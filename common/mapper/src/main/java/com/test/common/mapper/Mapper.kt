package com.test.common.mapper

interface Mapper<First, Second> {

    fun map(first: First): Second

}
