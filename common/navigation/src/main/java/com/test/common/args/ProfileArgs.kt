package com.test.common.args

import java.io.Serializable

data class ProfileArgs(val fullName: String, val userName: String,val isAdmin: Boolean) : Serializable