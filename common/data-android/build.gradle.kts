import dependencies.Libs


plugins {
    GradlePluginId.apply {
        id(ANDROID_LIBRARY)
        id(KOTLIN_ANDROID)
        id(PARCELIZE)
        id(KOTLIN_KAPT)
        id(KOTLIN_Hilt)
    }
}

android {
    GradleVersionConfig.apply {
        compileSdkVersion(COMPILE_SDK_VERSION)
        buildToolsVersion = BUILD_TOOLS_VERSION

        defaultConfig {
            minSdkVersion(MIN_SDK_VERSION)
            targetSdkVersion(TARGET_SDK_VERSION)
            versionCode = VERSION_CODE
            versionName = VERSION_NAME
            testInstrumentationRunner = TEST_INSTRUMENTATION_RUNNER

        }
         compileOptions.sourceCompatibility = JavaVersion.VERSION_1_8
        compileOptions.targetCompatibility = JavaVersion.VERSION_1_8
        kotlinOptions {
            jvmTarget = JavaVersion.VERSION_1_8.toString()
        }
    }
}



dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Libs.timber)
    implementation(Libs.AndroidX.Room.core)
    kapt(Libs.AndroidX.Room.compiler)
    implementation(Libs.AndroidX.LifeCycle.liveData)
    implementation(Libs.AndroidX.paging)
    implementation(Libs.Network.Retrofit.gsonConverter)
    implementation (Libs.DependencyInjection.Hilt.hilt)
    kapt (Libs.DependencyInjection.Hilt.compiler)
    kapt (Libs.DependencyInjection.Hilt.ViewModel.compiler)
    implementation (Libs.DependencyInjection.Hilt.ViewModel.hilt)

}