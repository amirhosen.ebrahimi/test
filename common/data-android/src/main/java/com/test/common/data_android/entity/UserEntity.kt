package com.test.common.data_android.entity

import android.content.SyncRequest
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(
    tableName = "user",
    primaryKeys = ["user_name"]
)
class UserEntity(
    @ColumnInfo(name = "full_name")
    val fullName: String ="",
    @ColumnInfo(name = "user_name")
    val userName: String="",
    @ColumnInfo(name = "password")
    val password: String="",
    @ColumnInfo(name = "is_admin")
    val isAdmin: Int=0
)
