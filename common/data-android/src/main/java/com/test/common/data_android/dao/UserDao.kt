package com.test.common.data_android.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.test.common.data_android.entity.UserEntity

@Dao
interface UserDao {
    @Query("SELECT * FROM user WHERE user_name=(:userName) AND password =(:password) ")
    suspend fun get(userName: String, password: String): UserEntity?

    @Query("SELECT * FROM user")
    suspend fun getAll(): List<UserEntity>

    @Insert
    suspend fun insert(data: List<UserEntity>)

    @Insert
    suspend fun insert(data: UserEntity)
}