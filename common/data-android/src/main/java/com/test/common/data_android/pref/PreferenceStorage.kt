package com.test.common.data_android.pref

import androidx.lifecycle.LiveData
import com.test.common.data_android.entity.UserEntity

interface PreferenceStorage {
    var session: UserEntity
//    var observableCurrentLocationTheme: LiveData<LocationEntity>
}