package com.test.common.sdkextentions.exceptions

class ServerException(message: String, val code: Int = -1) : Exception(message)
