package com.test.common.sdkextentions

/*
import com.test.common.sdkextentions.exceptions.ServerException
import org.json.JSONObject
import retrofit2.Response
import kotlin.jvm.Throws

@Throws(ServerException::class)
@Suppress("BlockingMethodInNonBlockingContext")
suspend inline fun <R> safeApiRequest(crossinline call: suspend () -> Response<R>): R {
    val response = call()
    if (response.isSuccessful) {
        return response.body()!!
    } else {
        val errorString = response.errorBody()!!.string()
        val errorBodyObject = JSONObject(errorString)
        val errorData = errorBodyObject.getJSONObject("meta")
        val errorMessage = errorData.getString("errorDetail")
        throw ServerException(errorMessage, response.code())
    }
}
*/
