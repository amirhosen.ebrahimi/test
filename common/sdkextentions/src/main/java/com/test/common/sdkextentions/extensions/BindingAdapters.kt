package com.test.common.sdkextentions.extensions

import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.View
import android.view.View.*
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextSwitcher
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.SearchView
import androidx.databinding.BindingAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.switchmaterial.SwitchMaterial
import com.test.common.data.*

@BindingAdapter("inVisibleOnLoadingResult")
fun View.inVisibleOnLoadingResult(result: Result<*>?) {
    visibility = if (result?.isLoading() != true)
        VISIBLE
    else
        INVISIBLE
}


@BindingAdapter("visibleOnLoadingResult")
fun View.visibleOnLoadingResult(result: Result<*>?) {
    visibility = if (result?.isLoading() == true)
        VISIBLE
    else
        INVISIBLE
}

