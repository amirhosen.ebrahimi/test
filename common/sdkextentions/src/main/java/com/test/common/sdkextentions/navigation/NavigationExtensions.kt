package com.test.common.sdkextentions.navigation

import androidx.navigation.NavOptions
import androidx.navigation.navOptions
import com.test.common.sdkextentions.R

val DefaultNavOptions: NavOptions = navOptions {
    anim {
        enter = R.anim.tivi_fade_scale_enter
        exit = R.anim.tivi_fade_exit
        popEnter = 0
        popExit = R.anim.tivi_fade_scale_exit
    }
}
