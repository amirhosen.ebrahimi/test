package com.test.common.sdkextentions.bindings

import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("imageUrl", "imagePlaceholder", requireAll = false)
fun ImageView.imageUrl(url: String?, @DrawableRes placeholderId: Int?) {
    Glide.with(context)
        .load(url)
        .apply {
            placeholderId?.let { ContextCompat.getDrawable(context, it) }
        }
        .into(this)
}
