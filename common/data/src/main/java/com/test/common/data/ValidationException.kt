package com.test.common.data

class ValidationException(message: String?) : Exception(message)