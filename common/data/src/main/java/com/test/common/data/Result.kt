package com.test.common.data


import com.test.common.data.Result.Loading
import com.test.common.data.Result.Error
import com.test.common.data.Result.Canceled
import com.test.common.data.Result.Success


sealed class Result<out R> {

    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val error: Throwable, var showDialog: Boolean = false) : Result<Nothing>()
    object Loading : Result<Nothing>()
    object Canceled : Result<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$error]"
            Loading -> "Loading"
            Canceled -> "Canceled"
        }
    }
}

inline fun <T> Result<T>.withResult(
    onLoading: (Boolean) -> Unit,
    onSuccess: (T) -> Unit,
    onFailure: (Throwable) -> Unit
) {
    when (this) {
        is Loading -> onLoading(true)
        is Canceled -> onLoading(false)
        is Error -> {
            onLoading(false)
            onFailure(error)
        }
        is Success -> {
            onLoading(false)
            onSuccess(data)
        }
    }
}

/**
 * [Success.data] if [Result] is of type [Success]
 */
fun <T> Result<T>.successOr(fallback: T): T {
    return (this as? Success<T>)?.data ?: fallback
}

fun <T> Result<T>.isLoading() = this is Loading
fun <T> Result<T>.isSuccess() = this is Success

inline fun <R> Result<R>.onSuccess(action: (R) -> Unit): Result<R> {
    if (this is Success) {
        action(data)
    }
    return this
}


/**
 * `true` if [Result] is of type [Success] & holds non-null [Success.data].
 */
val Result<*>.succeeded
    get() = this is Success && data != null



val <T> Result<T>.data: T?
    get() = (this as? Success)?.data



inline fun <R> Result<R>.onLoading(action: () -> Unit): Result<R> {
    if (this is Loading) {
        action()
    }
    return this
}

inline fun <T> Result<T>.onError(onFailure: (Throwable) -> Unit) {
    if (this is Error) onFailure(error)
}



