import dependencies.Libs
import dependencies.Gradle

object GradlePluginVersion {
    const val ANDROID_GRADLE = Gradle.androidGradleVersion
    const val KOTLIN = Libs.Kotlin.version
    const val SAFE_ARGS = Libs.AndroidX.Navigation.version
}

object Modules {
    const val CORE = ":core"

    object Feature {
        const val profiling = ":feature:profiling"
    }

    object Common {
        const val data = ":common:data"
        const val mapper = ":common:mapper"
        const val dataSource = ":common:datasource"
        const val dataAndroid = ":common:data-android"
        const val thread = ":common:thread"
        const val navigation = ":common:navigation"
        const val sdkExtensions = ":common:sdkextentions"
        const val useCase = ":common:usecase"
    }

}

object GradlePluginId {
    const val PARCELIZE = "kotlin-parcelize"
    const val KOTLIN_ANDROID = "kotlin-android"
    const val KOTLIN = "kotlin"
    const val JAVA_LIBRARY = "java-library"
    const val ANDROID_LIBRARY = "com.android.library"
    const val ANDROID_EXTENSIONS = "kotlin-android-extensions"
    const val ANDROID_DYNAMIC_FEATURE = "com.android.dynamic-feature"
    const val ANDROID_APPLICATION = "com.android.application"
    const val KOTLIN_KAPT = "kotlin-kapt"
    const val KOTLIN_Hilt = "dagger.hilt.android.plugin"
    const val FABRIC = "io.fabric"
}

object GradleOldWayPlugins {
    const val ANDROID_GRADLE =
        "com.android.tools.build:gradle:${GradlePluginVersion.ANDROID_GRADLE}"
    const val KOTLIN_GRADLE =
        "org.jetbrains.kotlin:kotlin-gradle-plugin:${GradlePluginVersion.KOTLIN}"
    const val SAFE_ARGS =
        "androidx.navigation:navigation-safe-args-gradle-plugin:${GradlePluginVersion.SAFE_ARGS}"
}

