package dependencies

// todo chek update library and more cleaner
object Libs {
    const val armadillo = "at.favre.lib:armadillo:0.11.0"
    const val imageCropper = "com.theartofdev.edmodo:android-image-cropper:2.8.0"
    const val material = "com.google.android.material:material:1.3.0-beta01"
    const val timber = "com.jakewharton.timber:timber:4.7.1"
    const val support = "com.android.support:support-v4:28.0.0"
    const val worker = "androidx.work:work-runtime:2.3.4"
    const val lotti = "com.airbnb.android:lottie:3.4.1"
    const val legacy = "androidx.legacy:legacy-support-v4:1.0.0"

    object DeviceName {
        const val deviceNames = "com.jaredrummler:android-device-names:2.0.0"
    }

    object GooglePlay {
        private const val play_core_version = "1.7.3"
        private const val play_core_ktx_version = "1.7.0"

        const val playCore = "com.google.android.play:core:$play_core_version"
        const val playCoreKtx = "com.google.android.play:core-ktx:$play_core_ktx_version"
    }



    object AndroidX {
        private const val paging_version = "3.0.0-alpha08"
        private const val fragment_version = "1.2.4"

        const val appCompat = "androidx.appcompat:appcompat:1.1.0"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:1.1.3"
        const val extensionsCore = "androidx.core:core-ktx:1.2.0"

        const val fragmentKtx = "androidx.fragment:fragment-ktx:$fragment_version"
        const val fragment = "androidx.fragment:fragment:$fragment_version"

        const val swipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout:1.0.0"
        const val worker = "androidx.work:work-runtime-ktx:2.3.4"
        const val paging = "androidx.paging:paging-runtime-ktx:$paging_version"
        const val annotation = "androidx.annotation:annotation:1.1.0"

        object LifeCycle {
            private const val version = "2.2.0"
            const val commonJava8 = "androidx.lifecycle:lifecycle-common-java8:$version"
            const val liveData = "androidx.lifecycle:lifecycle-livedata-ktx:$version"
            const val runtime = "androidx.lifecycle:lifecycle-runtime-ktx:$version"
            const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:$version"
            const val lifecycle_extensions = "androidx.lifecycle:lifecycle-extensions:$version"
        }

        object Navigation {
            const val version = "2.3.0-alpha06"
            const val core = "androidx.navigation:navigation-fragment-ktx:$version"
            const val ui = "androidx.navigation:navigation-ui-ktx:$version"
            const val dynamicFeature =
                "androidx.navigation:navigation-dynamic-features-fragment:$version"
        }

        object Room {
            private const val version = "2.3.0-alpha02"
            const val compiler = "androidx.room:room-compiler:$version"
            const val core = "androidx.room:room-ktx:$version"
            const val runtime = "androidx.room:room-runtime:$version"
        }
    }


    object DependencyInjection {
        object AssistedInject {
            private const val version = "0.5.2"
            const val annotations =
                "com.squareup.inject:assisted-inject-annotations-dagger2:$version"
            const val processor = "com.squareup.inject:assisted-inject-processor-dagger2:$version"
        }
        object Hilt {
            private const val version = "2.30.1-alpha"
            const val hilt = "com.google.dagger:hilt-android:${version}"
            const val compiler = "com.google.dagger:hilt-android-compiler:${version}"

            object ViewModel {
                private const val version = "1.0.0-alpha02"
                const val hilt = "androidx.hilt:hilt-lifecycle-viewmodel:${version}"
                const val compiler = "androidx.hilt:hilt-compiler:${version}"
            }
        }

        object Dagger {
            private const val version = "2.27"
            const val android = "com.google.dagger:dagger-android:$version"
            const val androidSupport = "com.google.dagger:dagger-android-support:$version"
            const val androidProcessor = "com.google.dagger:dagger-android-processor:$version"
            const val compiler = "com.google.dagger:dagger-compiler:$version"
        }
    }


    object Stetho {
        private const val version = "1.5.1"
        const val okHttp = "com.facebook.stetho:stetho-okhttp3:${version}"
        const val core = "com.facebook.stetho:stetho:${version}"
    }


    object Google {
        object Location {
            private const val version = "17.0.0"
            const val core = "com.google.android.gms:play-services-location:$version"
        }
    }

    object Glide {
        private const val version = "4.11.0"
        const val core = "com.github.bumptech.glide:glide:$version"
        const val compiler = "com.github.bumptech.glide:compiler:$version"
        const val okHttp = "com.github.bumptech.glide:okhttp3-integration:$version"
    }

    object Kotlin {
        const val version = "1.3.72"
        const val kotlin_stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$version"

        object Coroutine {
            private const val version = "1.4.2"
            const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
            const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$version"
            const val test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:$version"
        }
    }

    object Network {
        object OkHttp {
            private const val version = "4.7.0"
            const val core = "com.squareup.okhttp3:okhttp:$version"
            const val logger = "com.squareup.okhttp3:logging-interceptor:$version"
        }

        object Retrofit {
            private const val version = "2.8.1"
            const val core = "com.squareup.retrofit2:retrofit:$version"
            const val gsonConverter = "com.squareup.retrofit2:converter-gson:$version"
        }
    }

    object Scale {
        private const val version = "1.0.6"
        const val ssp = "com.intuit.ssp:ssp-android:$version"
        const val sdp = "com.intuit.sdp:sdp-android:$version"
    }


}

object Gradle {
    const val androidGradleVersion = "4.0.0"
}

object Tools {
    const val leakCanary = "com.squareup.leakcanary:leakcanary-android:2.2"

    object Hyperion {
        private const val version = "0.9.27"
        const val attr = "com.willowtreeapps.hyperion:hyperion-attr:$version"
        const val buildConfig = "com.willowtreeapps.hyperion:hyperion-build-config:$version"
        const val crash = "com.willowtreeapps.hyperion:hyperion-crash:$version"
        const val core = "com.willowtreeapps.hyperion:hyperion-core:$version"
        const val disk = "com.willowtreeapps.hyperion:hyperion-disk:$version"
        const val geigerCounter = "com.willowtreeapps.hyperion:hyperion-geiger-counter:$version"
        const val measurement = "com.willowtreeapps.hyperion:hyperion-measurement:$version"
        const val phoenix = "com.willowtreeapps.hyperion:hyperion-phoenix:$version"
        const val preferences = "com.willowtreeapps.hyperion:hyperion-shared-preferences:$version"
        const val recorder = "com.willowtreeapps.hyperion:hyperion-recorder:$version"
        const val timber = "com.willowtreeapps.hyperion:hyperion-timber:$version"
    }
}

object Test {
    private const val coreVersion = "1.0.0"
    private const val runnerVersion = "1.1.1"
    private const val robolectricVersion = "4.3.1"
    private const val junitVersion = "4.12"


    const val junit = "junit:junit:$junitVersion"
    const val extJunit = "androidx.test.ext:junit:$runnerVersion"
    const val robolectricEnv = "androidx.test:core:$coreVersion"
    const val robolectric = "org.robolectric:robolectric:$robolectricVersion"
    const val runner = "androidx.test:runner:$runnerVersion"
    const val rules = "androidx.test:rules:$runnerVersion"

    object Espresso {
        private const val version = "3.1.0"
        const val core = "androidx.test.espresso:espresso-core:$version"
        const val contrib = "androidx.test.espresso:espresso-contrib:$version"
        const val intents = "androidx.test.espresso:espresso-intents:$version"
        const val accessibility = "androidx.test.espresso:espresso-accessibility:$version"
        const val web = "androidx.test.espresso:espresso-web:$version"
        const val concurrent = "androidx.test.espresso.idling:idling-concurrent:$version"
    }

    object Mockito {
        private const val mockitoKotlinVersion = "2.2.0"
        private const val mockitoCoreVersion = "1.10.19"
        const val mockito = "org.mockito:mockito-core:$mockitoCoreVersion"
        const val mockitoKotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:$mockitoKotlinVersion"
    }
}
