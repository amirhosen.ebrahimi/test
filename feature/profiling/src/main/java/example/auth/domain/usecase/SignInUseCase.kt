package example.auth.domain.usecase

import com.test.common.data.ValidationException
import com.test.common.mapper.Mapper
import com.test.common.thread.IoDispatcher
import com.test.common.usecase.CoroutineUseCase
import example.auth.domain.entity.User
import example.auth.domain.repository.UserRepository
import example.auth.presentation.entity.UserView
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class SignInUseCase @Inject constructor(
    @IoDispatcher dispatcher: CoroutineDispatcher,
    private val userRepository: UserRepository,
    private val mapper: Mapper<User, UserView>
) : CoroutineUseCase<SignInUseCase.Params, UserView>(dispatcher) {

    override suspend fun execute(parameters: Params): UserView {
        if (parameters.userName.isEmpty() || parameters.password.isEmpty())
            throw ValidationException("All fields are required")

        val user = userRepository.get(
            userName = parameters.userName,
            password = parameters.password
        )
            ?: throw ValidationException("userName or password is wrong !")

        return mapper.map(
            user
        )
    }


    data class Params(val userName: String, val password: String)
}