package example.auth.domain.usecase

import com.test.common.data.ValidationException
import com.test.common.data_android.entity.UserEntity
import com.test.common.mapper.Mapper
import com.test.common.thread.IoDispatcher
import com.test.common.usecase.CoroutineUseCase
import example.auth.domain.entity.User
import example.auth.domain.repository.UserRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class SignUpUseCase @Inject constructor(
    @IoDispatcher dispatcher: CoroutineDispatcher,
    private val userRepository: UserRepository,
    private val mapper: Mapper<User, UserEntity>
) : CoroutineUseCase<SignUpUseCase.Params, Unit>(dispatcher) {

    override suspend fun execute(parameters: Params) {
        if (parameters.userName.isEmpty() || parameters.password.isEmpty())
            throw ValidationException("All fields are required")
        if (parameters.password != parameters.confirmPassword)
            throw ValidationException("confirm password is wrong")

        userRepository.insert(
            mapper.map(
                User(
                    fullName = parameters.fullName,
                    userName = parameters.userName,
                    password = parameters.password,
                    isAdmin = false
                )
            )
        )
    }


    data class Params(
        val fullName: String,
        val userName: String,
        val password: String,
        val confirmPassword: String
    )
}