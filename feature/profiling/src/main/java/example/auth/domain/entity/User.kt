package example.auth.domain.entity

data class User(val fullName : String,val userName:String,val password : String,val isAdmin : Boolean)