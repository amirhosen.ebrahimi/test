package example.auth.domain.di

import com.test.common.data_android.entity.UserEntity
import com.test.common.mapper.Mapper
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import example.auth.data.repository.UserRepositoryImpl
import example.auth.domain.entity.User
import example.auth.domain.mapper.UserToUserEntity
import example.auth.domain.mapper.UserToUserViewMapper
import example.auth.domain.repository.UserRepository
import example.auth.presentation.entity.UserView

@InstallIn(SingletonComponent::class)
@Module
abstract class DomainModule {
    @Binds
    abstract fun provideUserRepository(repository: UserRepositoryImpl): UserRepository

    // ...................mapper.......................
    @Binds
    abstract fun provideUserToUserViewMapper(mapper: UserToUserViewMapper): Mapper<User, UserView>

    @Binds
    abstract fun provideUserToUserEntity(mapper: UserToUserEntity): Mapper<User, UserEntity>
}
