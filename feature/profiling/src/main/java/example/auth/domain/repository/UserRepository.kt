package example.auth.domain.repository

import com.test.common.data_android.entity.UserEntity
import example.auth.domain.entity.User

interface UserRepository {
    suspend fun get(userName: String, password: String): User?
    suspend fun insert(userEntity: UserEntity)
}