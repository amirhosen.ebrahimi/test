package example.auth.domain.mapper

import com.test.common.mapper.Mapper
import example.auth.domain.entity.User
import example.auth.presentation.entity.UserView
import javax.inject.Inject

class UserToUserViewMapper @Inject constructor() : Mapper<User, UserView> {
    override fun map(first: User): UserView {
        return UserView(first.fullName)
    }
}