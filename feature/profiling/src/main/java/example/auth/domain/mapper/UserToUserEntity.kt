package example.auth.domain.mapper

import com.test.common.data_android.entity.UserEntity
import com.test.common.mapper.Mapper
import com.test.common.sdkextentions.extensions.toInt
import example.auth.domain.entity.User
import javax.inject.Inject

class UserToUserEntity @Inject constructor() : Mapper<User, UserEntity> {
    override fun map(first: User): UserEntity {
        return UserEntity(
            fullName = first.fullName,
            userName = first.userName,
            password = first.password,
            isAdmin = first.isAdmin.toInt()
        )
    }

}
