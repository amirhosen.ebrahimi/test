package example.auth.presentation.entity

data class UserView(val fullName: String)