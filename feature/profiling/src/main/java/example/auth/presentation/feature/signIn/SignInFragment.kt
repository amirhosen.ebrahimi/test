package example.auth.presentation.feature.signIn

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.near.feature.placelist.R
import com.near.feature.placelist.databinding.SignInFragmentBinding
import com.test.common.data.onError
import com.test.common.data.onSuccess
import com.test.common.navigation.Navigations
import com.test.common.sdkextentions.extensions.toUri
import com.test.common.sdkextentions.properties.viewBinding
import dagger.hilt.android.AndroidEntryPoint

// todo loading
@AndroidEntryPoint
class SignInFragment : Fragment(R.layout.sign_in_fragment) {

    private val viewModel by viewModels<SignInViewModel>()

    private val binding by viewBinding(SignInFragmentBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            vm = viewModel
        }
        viewModel.loginLiveData.observe(viewLifecycleOwner, Observer { it ->
            it.onError { messageId ->
                Toast.makeText(requireContext(), messageId.message, Toast.LENGTH_LONG).show()
            }
            it.onSuccess {
                Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_LONG).show()
            }
        })
        binding.btnLogin.setOnClickListener {
            viewModel.login(
                binding.userNameField.text.toString()
                , binding.passwordField.text.toString()
            )
        }
        binding.btnSignUp.setOnClickListener {
            findNavController().navigate(Navigations.signUp.toUri())
        }
    }


}