package example.auth.presentation.feature.signIn

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import example.auth.domain.usecase.SignInUseCase
import example.auth.presentation.entity.UserView
import com.test.common.data.Result
import com.test.common.data_android.dao.UserDao
import com.test.common.data_android.entity.UserEntity
import kotlinx.coroutines.launch

class SignInViewModel @ViewModelInject constructor(
    private val signInUseCase: SignInUseCase,
    private val userDao: UserDao
) : ViewModel() {
    private val _loginLiveData = MutableLiveData<Result<UserView>>()
    val loginLiveData: LiveData<Result<UserView>>
        get() = _loginLiveData

    fun login(userName: String, password: String) {
        viewModelScope.launch {
            addData()
            _loginLiveData.postValue(Result.Loading)
            _loginLiveData.postValue(signInUseCase(SignInUseCase.Params(userName, password)))
        }
    }

    private suspend fun addData() {
        val data = userDao.getAll()
        if (data.isEmpty())
            userDao.insert(
                listOf(
                    UserEntity(
                        fullName = "amir",
                        userName = "amir",
                        password = "123",
                        isAdmin = 1
                    ), UserEntity(fullName = "amir2",userName = "amir2", password = "123", isAdmin = 0)
                )
            )
    }
}
