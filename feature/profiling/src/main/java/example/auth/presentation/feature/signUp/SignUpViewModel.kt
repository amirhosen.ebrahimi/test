package example.auth.presentation.feature.signUp

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.common.data.Result
import example.auth.domain.usecase.SignUpUseCase
import kotlinx.coroutines.launch

class SignUpViewModel @ViewModelInject constructor(private val signUpUseCase: SignUpUseCase) :
    ViewModel() {
    private val _signUpLiveData = MutableLiveData<Result<Unit>>()
    val signUpLiveData: LiveData<Result<Unit>>
        get() = _signUpLiveData

    fun signUp(fullName: String, userName: String, password: String, confirmPassword: String) {
        viewModelScope.launch {
            _signUpLiveData.postValue(Result.Loading)
            _signUpLiveData.postValue(
                signUpUseCase(
                    SignUpUseCase.Params(
                        fullName = fullName,
                        userName = userName,
                        password = password,
                        confirmPassword = confirmPassword
                    )
                )
            )
        }
    }
}
