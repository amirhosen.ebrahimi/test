package example.auth.presentation.feature.signUp

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.near.feature.placelist.R
import com.near.feature.placelist.databinding.SignUpFragmentBinding
import com.test.common.data.onError
import com.test.common.data.onSuccess
import com.test.common.sdkextentions.properties.viewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpFragment : Fragment(R.layout.sign_up_fragment) {

    private val viewModel by viewModels<SignUpViewModel>()

    private val binding by viewBinding(SignUpFragmentBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            vm = viewModel
        }
        viewModel.signUpLiveData.observe(viewLifecycleOwner, Observer {
            it.onError {
                if (it is SQLiteConstraintException) {
                    Toast.makeText(
                        requireContext(),
                        R.string.repetitious_username_error,
                        Toast.LENGTH_SHORT
                    ).show()
                } else
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
            }
            it.onSuccess {
                Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
            }
        })
        binding.btnSignUp.setOnClickListener {
            viewModel.signUp(
                binding.fullNameField.text.toString(),
                binding.userNameField.text.toString(),
                binding.passwordField.text.toString(),
                binding.confirmPasswordField.text.toString()
            )
        }
    }
}