package example.auth.data.di

import com.test.common.data_android.entity.UserEntity
import com.test.common.mapper.Mapper
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import example.auth.data.dataSource.LocalUserDataSource
import example.auth.data.dataSource.LocalUserDataSourceReadable
import example.auth.data.dataSource.LocalUserDataSourceWritable
import example.auth.data.mapper.UserEntityToUser
import example.auth.domain.entity.User

@InstallIn(SingletonComponent::class)
@Module
abstract class DataModule {
    //......................Mappers...........................
    @Binds
    abstract fun bindUserEntityToUser(dataSource: UserEntityToUser): Mapper<UserEntity?, User?>

    //......................DataSources...........................
    @Binds
    abstract fun bindLocalUserDataSourceReadable(dataSource: LocalUserDataSource): LocalUserDataSourceReadable

    @Binds
    abstract fun bindLocalUserDataSourceWritable(dataSource: LocalUserDataSource): LocalUserDataSourceWritable

}