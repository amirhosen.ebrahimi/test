package example.auth.data.repository

import com.test.common.data_android.entity.UserEntity
import com.test.common.mapper.Mapper
import example.auth.data.dataSource.LocalUserDataSourceReadable
import example.auth.data.dataSource.LocalUserDataSourceWritable
import example.auth.domain.entity.User
import example.auth.domain.repository.UserRepository
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val mapper: Mapper<UserEntity?, User?>,
    private val localUserDataSourceReadable: LocalUserDataSourceReadable,
    private val localUserDataSourceWritable: LocalUserDataSourceWritable
) :
    UserRepository {
    override suspend fun get(userName: String, password: String): User? {
        return mapper.map(
            localUserDataSourceReadable.read(
                LocalUserDataSourceReadable.Params(
                    userName,
                    password
                )
            )
        )
    }

    override suspend fun insert(
        userEntity: UserEntity
    ) {
        localUserDataSourceWritable.write(userEntity)
    }
}