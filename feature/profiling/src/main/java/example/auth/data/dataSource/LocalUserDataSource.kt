package example.auth.data.dataSource

import com.test.common.data_android.dao.UserDao
import com.test.common.data_android.entity.UserEntity
import com.test.common.datasource.Readable
import com.test.common.datasource.Writable
import javax.inject.Inject

class LocalUserDataSource @Inject constructor(private val userDao: UserDao) :
    LocalUserDataSourceReadable,
    LocalUserDataSourceWritable{
    override suspend fun read(input: LocalUserDataSourceReadable.Params): UserEntity? {

        return userDao.get(input.userName, input.password)
    }

    override suspend fun write(input: UserEntity) {
        userDao.insert(input)
    }


}


interface LocalUserDataSourceReadable :
    Readable.Suspendable.IO<LocalUserDataSourceReadable.Params, UserEntity?> {
    data class Params(val userName: String, val password: String)
}
typealias LocalUserDataSourceWritable = Writable.Suspendable<UserEntity>