package example.auth.data.mapper

import com.test.common.data_android.entity.UserEntity
import com.test.common.mapper.Mapper
import com.test.common.sdkextentions.extensions.toBoolean
import example.auth.domain.entity.User
import javax.inject.Inject

class UserEntityToUser @Inject constructor() : Mapper<UserEntity?, User?> {
    override fun map(first: UserEntity?): User? {
        if (first == null) return null
        return User(fullName = first.userName,password = first.password,userName = first.userName,isAdmin = first.isAdmin.toBoolean())
    }

}