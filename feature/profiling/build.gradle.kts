import dependencies.Libs


plugins {
    GradlePluginId.apply {
        id(ANDROID_LIBRARY)
        id(KOTLIN_ANDROID)
        id(ANDROID_EXTENSIONS)
        id(KOTLIN_KAPT)
        id(KOTLIN_Hilt)
    }
}

kapt {
    correctErrorTypes = true
    useBuildCache = true

    javacOptions {
        option("-Xmaxerrs", 2000)
    }
}

android {
    GradleVersionConfig.apply {

        compileSdkVersion(COMPILE_SDK_VERSION)
        buildToolsVersion = BUILD_TOOLS_VERSION
        defaultConfig {
            minSdkVersion(MIN_SDK_VERSION)
            targetSdkVersion(TARGET_SDK_VERSION)
            versionCode = VERSION_CODE
            versionName = VERSION_NAME
            testInstrumentationRunner = TEST_INSTRUMENTATION_RUNNER

        }

        android.buildFeatures.dataBinding = true
        compileOptions.sourceCompatibility = JavaVersion.VERSION_1_8
        compileOptions.targetCompatibility = JavaVersion.VERSION_1_8

        kotlinOptions {
            jvmTarget = JavaVersion.VERSION_1_8.toString()
            freeCompilerArgs =
                freeCompilerArgs.toMutableList().apply { add("-Xopt-in=kotlin.RequiresOptIn") }
        }
    }
}
//todo delete extra hilt library
dependencies {
    implementation(project(Modules.Common.dataAndroid))
    implementation(project(Modules.Common.useCase))
    implementation(project(Modules.Common.sdkExtensions))
    implementation(project(Modules.Common.data))
    implementation(project(Modules.Common.dataSource))
    implementation(project(Modules.Common.navigation))
    implementation(project(Modules.Common.mapper))
    implementation(project(Modules.Common.thread))








    implementation (Libs.DependencyInjection.Hilt.hilt)

    kapt (Libs.DependencyInjection.Hilt.compiler)

    implementation (Libs.DependencyInjection.Hilt.ViewModel.hilt)
    kapt (Libs.DependencyInjection.Hilt.ViewModel.compiler)



    implementation(Libs.timber)
    implementation(Libs.AndroidX.fragmentKtx)
    implementation(Libs.AndroidX.fragment)
    implementation(Libs.AndroidX.Room.core)
    implementation(Libs.AndroidX.LifeCycle.commonJava8)
    implementation(Libs.AndroidX.LifeCycle.liveData)
    implementation(Libs.AndroidX.LifeCycle.runtime)
    implementation(Libs.AndroidX.LifeCycle.viewModel)
    implementation(Libs.material)
    implementation(Libs.AndroidX.extensionsCore)
    implementation(Libs.Kotlin.kotlin_stdlib)
    implementation(Libs.AndroidX.LifeCycle.lifecycle_extensions)
    implementation(Libs.Scale.sdp)
    implementation(Libs.Scale.ssp)
    implementation(Libs.AndroidX.constraintLayout)
    implementation(Libs.AndroidX.Navigation.core)
    implementation(Libs.AndroidX.Navigation.ui)
    implementation(Libs.Kotlin.Coroutine.core)
    implementation(Libs.Kotlin.Coroutine.android)
    implementation("com.android.support:support-v4:28.0.0")

    kapt(Libs.AndroidX.Room.compiler)


}